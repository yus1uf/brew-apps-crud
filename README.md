# brew-apps-crud


## Refer Below for API Documentation
 - [Click Here](https://documenter.getpostman.com/view/17963881/2s9YRGxpFa) ||https://documenter.getpostman.com/view/17963881/2s9YRGxpFa

## Deployed Server Link

### [Click](https://brew-apps.onrender.com/api/v1) [https://brew-apps-crud.onrender.com/api/v1]
## Defined Routes
- ### For Books Crud Operation
  - /books/add
  - /books/get-all
  - /books/get/:id
  - /books/update/:id
  - /books/delete/:id



## Getting started With locally

To make it easy for you, here's a list of recommended next steps.


## Clone Project

```
cd existing_repo
git clone https://gitlab.com/yus1uf/brew-apps-crud.git
cd brew-apps-crud
touch .env (copy .env.sample file content and paste and add value)
npm i
npm start
```

## Deployment on AWS
 - Create EC2 instance and donwnload .pem file
 - ssh into your instance
 - install git, node v20, npm, and pm2
 - execute the below command to run
   ```
   cd var
   mkdir www
   cd www
   git clone https://gitlab.com/yus1uf/brew-apps-crud.git
   cd brew-apps-crud
   touch .env (copy .env.sample file content and paste and add value)
   cd src
   pm2 start index.js
   ```