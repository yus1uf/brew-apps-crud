import { BookModel } from "../database/model/book.model.js";
import { customResponse } from "../utilities/helper/custom.response.js";
import { paramsValidator, bodyValidator } from "../utilities/helper/schema/joi.bookSchema.js";
import { Messages, Codes } from "../utilities/constant/constant.js";

export const addBook = async(req, res) =>{
try {
    const body = req.body
    const validate = bodyValidator(body)
    if (validate.error) {
       const response = customResponse({code: Codes.BAD_REQUEST, message: validate.error.message, err: validate.error.details[0]})
       return res.status(Codes.BAD_REQUEST).send(response)
    }

    const bookDeatils = await new BookModel({...body}).save()

    const response = customResponse({code: Codes.CREATED, message: Messages.CREATED, data: bookDeatils})
    return res.status(Codes.CREATED).send(response)
} catch (error) {
    console.log(error);
    const response = customResponse({code: Codes.SERVER_ERROR, message: Messages.SERVER_ERROR, err: error})
    return res.status(Codes.SERVER_ERROR).send(response)
}
}

export const listBooks = async(req,res) =>{
    try {
        const books = await BookModel.find()
        if (books.length === 0) {
            const response = customResponse({code: Codes.SUCCESS, message: Messages.NOT_FOUND, data: books})
            return res.status(Codes.SUCCESS).send(response)
        }
        const response = customResponse({code:Codes.SUCCESS, message: Messages.FETCHED, data: books})
        return res.status(Codes.SUCCESS).send(response)
    } catch (error) {
        console.log(error);
        const response = customResponse({code: Codes.SERVER_ERROR, message: Messages.SERVER_ERROR, err: error})
        return res.status(Codes.SERVER_ERROR).send(response)
    }
}

export const listBookById = async(req,res) => {
   try {
    const id = req.params.id
    const validate = paramsValidator(id)
    if (validate.error) {
       const response = customResponse({code: Codes.BAD_REQUEST, message: validate.error.message, err: validate.error.details[0]})
       return res.status(Codes.BAD_REQUEST).send(response)
    }
    const book = await BookModel.findById(id)
    if (!book) {
        const response = customResponse({code: Codes.SUCCESS, message: Messages.NOT_FOUND, data: book})
        return res.status(Codes.SUCCESS).send(response) 
    }
    const response = customResponse({code:Codes.SUCCESS, message: Messages.FETCHED, data: book})
    return res.status(Codes.SUCCESS).send(response)
   } catch (error) {
    console.log(error);
    const response = customResponse({code: Codes.SERVER_ERROR, message: Messages.SERVER_ERROR, err: error})
    return res.status(Codes.SERVER_ERROR).send(response)
   }
}

export const updateBook = async(req,res) =>{
    try {
        const body = req.body 
        const id = req.params.id
        const bodyValidate = bodyValidator(body)
        const paramsValidate = paramsValidator(id)
        if (bodyValidate.error) {
           const response = customResponse({code: Codes.BAD_REQUEST, message: bodyValidate.error.message, err:  bodyValidate.error.details[0]})
           return res.status(Codes.BAD_REQUEST).send(response)
        }
        if (paramsValidate.error) {
            const response = customResponse({code: Codes.BAD_REQUEST, message: paramsValidate.error.message, err: paramsValidate.error.details[0]})
            return res.status(Codes.BAD_REQUEST).send(response)
         }
         const book = await BookModel.findById(id)
         if (!book) {
            const response = customResponse({code: Codes.SUCCESS, message: Messages.NOT_FOUND, data: book})
            return res.status(Codes.SUCCESS).send(response) 
        }
        const updatedDetails = await BookModel.findByIdAndUpdate(id,{...body, updatedAt: Date.now()}, {new:true})
        await updatedDetails.save()
        const response = customResponse({code: Codes.SUCCESS, message: Messages.UPDATED, data: updatedDetails})
        return res.status(Codes.SUCCESS).send(response)
    } catch (error) {
        console.log(error);
        const response = customResponse({code: Codes.SERVER_ERROR, message: Messages.SERVER_ERROR, err: error})
        return res.status(Codes.SERVER_ERROR).send(response)
    }
}

export const deleteBook = async(req,res) => {
    try {
        const id = req.params.id
        const validate = paramsValidator(id)
        if (validate.error) {
        const response = customResponse({code: Codes.BAD_REQUEST, message: validate.error.message, err: validate.error.details[0]})
        return res.status(Codes.BAD_REQUEST).send(response)
        }
        const book = await BookModel.findById(id)
        if (!book) {
           const response = customResponse({code: Codes.SUCCESS, message: Messages.NOT_FOUND, data: book})
           return res.status(Codes.SUCCESS).send(response) 
       }
        const deletedDetails = await BookModel.findByIdAndDelete(id)
        const response = customResponse({code: Codes.SUCCESS, message: Messages.DELETED, data: deletedDetails})
        return res.status(Codes.SUCCESS).send(response)

    } catch (error) {
        console.log(error);
        const response = customResponse({code: Codes.SERVER_ERROR, message: Messages.SERVER_ERROR, err: error})
        return res.status(Codes.SERVER_ERROR).send(response) 
    }
}