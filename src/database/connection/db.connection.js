import mongoose from "mongoose"



export const DBConnection = () => {
  mongoose.connect(`${process.env.DB_URL}/${process.env.DB_NAME}`);
  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error: "));
  db.once("open", function () {
    console.log(`${process.env.DB_NAME} database connected successfully!!`);
  });
};
