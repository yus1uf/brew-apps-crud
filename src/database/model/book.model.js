import mongoose from "mongoose";

const bookSchema = new mongoose.Schema({
  title: {
    type: String,
    require: true,
  },
  author: {
    type: String,
    require: true,
  },
  summary: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
updatedAt: {
    type: Date
}
});

export const BookModel = mongoose.model("books", bookSchema);
