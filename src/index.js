import express from 'express'
import { DBConnection } from './database/connection/db.connection.js';
import router from './routes/index.js';
const app = express();


const port = process.env.PORT || 8085


app.use(express.json());

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use('/api/v1', router);

app.use("*", (req,res)=>{
    return res.status(200).json({
        app: "Brew Apps Assesment",
        delployedUrl: "https://brew-apps.onrender.com/api/v1",
        devUrl: "http://localhost:4000/api/v1",
        routes: ["/books/add","/books/get-all","/books/get/:id","/books/update/:id","/books/delete/:id"]
    })
})

app.listen(port, () => {
  DBConnection()
  console.log(`app listening on port ${port}`);
});
