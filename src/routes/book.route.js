import express from 'express'
import {addBook,listBooks,listBookById,updateBook,deleteBook} from '../controller/book.controller.js'

export const booksRouter = express.Router()


booksRouter.post('/add', addBook)
booksRouter.get('/get-all', listBooks)
booksRouter.get('/get/:id', listBookById)
booksRouter.put('/update/:id', updateBook)
booksRouter.delete('/delete/:id', deleteBook)
