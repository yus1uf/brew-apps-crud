import { Router } from "express";

import { booksRouter } from "./book.route.js";


const router = Router()

router.use('/books', booksRouter)


export default router
