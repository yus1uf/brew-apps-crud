export const Messages = {
    CREATED: "book created successfully!!",
    SERVER_ERROR: "something went wrong!!",
    NOT_FOUND: "no books found in database!!",
    FETCHED: "books fetched successfully!!",
    UPDATED: "books details updated successfully!!",
    DELETED: "books details deleted successfully!!"
}

export const Codes = {
    SUCCESS: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    SERVER_ERROR: 500
}