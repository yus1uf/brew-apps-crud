export const customResponse = ({
    code = 200,
    message = "",
    data = {},
    err = {},
  }) => {
    const responseStatus = code < 300 ? "success" : "failure";
    return {
      status: responseStatus,
      code,
      data,
      message,
      error: err,
    };
  };
