import Joi from "joi";

export const paramsValidator = (_id) => {
    const id = Joi.string()
      .required()
      .regex(/^[0-9a-fA-F]{24}$/, "object Id");
  
    return id.validate(_id);
  };

export const bodyValidator = (body) => {
    const schema = Joi.object().keys({
        title: Joi.string().required(),
        author: Joi.string().required().min(3),
        summary: Joi.string(),
      });
    
      return schema.validate(body);
}